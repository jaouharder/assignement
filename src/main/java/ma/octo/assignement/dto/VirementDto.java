package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

public class VirementDto {

    private String numeroCompteEmetteur;

    private String numeroCompteBeneficiaire;

    private String motif;

    private BigDecimal montantVirement;

    private Instant date; //Instant

    public VirementDto(String numeroCompteEmetteur, String numeroCompteBeneficiaire, String motif, BigDecimal montantVirement, Instant date) {
        this.numeroCompteEmetteur = numeroCompteEmetteur;
        this.numeroCompteBeneficiaire = numeroCompteBeneficiaire;
        this.motif = motif;
        this.montantVirement = montantVirement;
        this.date = date;
    }

    public String getNumeroCompteEmetteur() {
        return numeroCompteEmetteur;
    }

    public String getNumeroCompteBeneficiaire() {
        return numeroCompteBeneficiaire;
    }

    public BigDecimal getMontantVirement() {
        return montantVirement;
    }

    public String getMotif() {
        return motif;
    }


    public Instant getDate() {
        return date;
    }
}
