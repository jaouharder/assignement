package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

//TODO we should replace sysout with logger.info/warn/error
@RestController(value = "/virements")
@Validated
class mainController {


    Logger LOGGER = LoggerFactory.getLogger(mainController.class);

    @Autowired
    private CompteService compteService;
    @Autowired
    private VirementService virementService;
    @Autowired
    private UtilisateurService utilisateurService;
    @Autowired
    private VersementService versementService;

    @GetMapping("lister_virements")
    List<VirementDto> loadAllVirements() {
        return virementService.getAllVirements();
    }

    @GetMapping("lister_versements")
    List<VersementDto> loadAllVersements() {
        return versementService.getAllVersements();
    }

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
        return compteService.getAllComptes();
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.getAllUtilisateurs();
    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(
            @Valid @RequestParam @NotEmpty(message = "Pas de compte emetteur") String numeroCompteEmetteur,
            @Valid @RequestParam @NotEmpty(message = "Pas de compte beneficiaire") String numeroCompteBeneficiaire,
            @Valid @RequestParam @NotEmpty(message = "Pas de motif") String motif,
            @Valid @RequestParam @Min(value = 10, message = "Montant minimale non atteint (10)") @Max(value = 10000, message = "Montant maximale atteint (10000)") BigDecimal montantVirement,
            @RequestParam Instant date
    ) throws SoldeDisponibleInsuffisantException, CompteNonExistantException {
        virementService.createVirement(new VirementDto(numeroCompteEmetteur, numeroCompteBeneficiaire, motif, montantVirement, date));
    }

    @PostMapping("/executerVerements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVersement(
            @Valid @RequestParam @NotEmpty(message = "Pas du nom emetteur") String nomPrenomEmetteur,
            @Valid @RequestParam @NotEmpty(message = "Pas de compte beneficiaire") String rib,
            @Valid @RequestParam @NotEmpty(message = "Pas de motif") String motifVersement,
            @Valid @RequestParam @Min(value = 10, message = "Montant minimale non atteint (10)") @Max(value = 10000, message = "Montant maximale atteint (10000)") BigDecimal montantVersement,
            @RequestParam Instant dateExecution
    ) throws CompteNonExistantException {
        versementService.createVersement(new VersementDto(montantVersement, dateExecution, nomPrenomEmetteur, rib, motifVersement));
    }


}
